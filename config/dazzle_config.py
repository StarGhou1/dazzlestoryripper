from os import getcwd, path
from auxiliary.color_utils import *


class DazzleConfig:
    def __init__(self):
        self._archive_link = "https://pastebin.com/raw/aXsg1jmE"
        self._output_folder = f"{getcwd()}/StoryArchive"
        self._scrape_authors = True

    @property
    def archive_link(self):
        return self._archive_link

    @property
    def output_folder(self):
        return self._output_folder

    @property
    def scrape_authors(self):
        return self._scrape_authors

    @output_folder.setter
    def output_folder(self, new_path):
        if path.exists(new_path):
            self.output_folder = new_path
            print_info(f"Output Folder is now {self.output_folder}")
        else:
            print_error("Error: Supplied path does not exist. Output Folder will keep its current value.")
        input("Enter anything to continue...")

    def print_current_options(self):
        print_general("-=Current Options=-")
        print_info(f"Archive Link: {self._archive_link} (Hardcoded)")
        print_info(f"(1) Output Folder: {self._output_folder}")
        print_info(f"(2) Scrape Authors: {self._scrape_authors}")

    def do_options_menu(self):
        selection = input(
            "Input the number of the option you want to change, or press Enter to continue: "
        )
        if selection == "1":
            new_output_path = input("Enter new output folder path: ")
            self.output_folder = new_output_path
        elif selection == "2":
            self._scrape_authors = not self._scrape_authors
        elif selection == "":
            return True
        return False
