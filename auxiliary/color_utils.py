from colorama import Fore, Style


C_GENERAL = Fore.GREEN
C_ERROR = Fore.RED
C_WARNING = Fore.YELLOW
C_INFO = Fore.CYAN
C_QUESTION = Fore.LIGHTBLUE_EX


def _template_(c_meaning, symbol, output):
    return f"{Fore.BLUE}[{c_meaning}{symbol}{Fore.BLUE}]{c_meaning} {output} {Style.RESET_ALL}"


def print_general(output):
    print(_template_(C_GENERAL, '=', output))


def print_info(output):
    print(_template_(C_INFO, '~', output))


def print_warning(output):
    print(_template_(C_WARNING, '!', output))


def print_question(output):
    print(_template_(C_QUESTION, '?', output))


def print_error(output):
    print(_template_(C_ERROR, '-', output))
