import re
from requests import get
from bs4 import BeautifulSoup
from os import makedirs
from auxiliary.color_utils import *
from config.dazzle_config import DazzleConfig


class Scraper:
    def __init__(self, config: DazzleConfig):
        self.config = config
        self._archive_request = get(self.config.archive_link)
        self._archive_parsed = BeautifulSoup(self._archive_request.content, features="html.parser")
        self._archive_lines = self._archive_parsed.prettify().split('\n')

    def create_folder(self, name=None):
        if name is not None:
            folder_path = f"{self.config.output_folder}/{name}"
        else:
            folder_path = self.config.output_folder
        makedirs(folder_path, exist_ok=True)
        return folder_path

    @staticmethod
    def get_paste_author(paste_link):
        paste_request = get(paste_link)
        paste_parsed = BeautifulSoup(paste_request.content, features="html.parser")
        try:
            paste_author = paste_parsed.find("div", "paste_box_line2").find("a").contents[0]
        except AttributeError:  # Occurs if a link to the author doesn't exist, in which case, it's a guest paste.
            paste_author = "Anonymous"
        return paste_author

    def do_paste_ripping(self, paste_links):
        for paste_link in paste_links:
            paste_request = get(paste_link)
            paste_parsed = BeautifulSoup(paste_request.content, features="html.parser")
            paste_title = paste_parsed.find("div", "paste_box_line1").get("title").replace('/', '~')
            paste_author = Scraper.get_paste_author(paste_link)

            print_info(f"Downloading '{paste_title}' by '{paste_author}' into {self.config.output_folder}")

            paste_id = paste_link.split("pastebin.com/")[1]
            raw_link = f"https://pastebin.com/raw/{paste_id}"
            raw_request = get(raw_link)
            raw_parsed = BeautifulSoup(raw_request.content, features="html.parser").text

            with open(f"{self.config.output_folder}/{paste_title} by {paste_author}.txt", "w+") as paste_file:
                paste_file.write(raw_parsed)

    def do_author_ripping(self, author_links):
        for author_link in author_links:
            author_name = author_link.split("/u/")[1]
            print_info(f"Downloading {author_name}'s pastes into {self.config.output_folder}/{author_name}")
            self.create_folder(author_name)
            author_request = get(author_link)
            author_parsed = BeautifulSoup(author_request.content, features="html.parser")
            content_table = author_parsed.find("table", attrs={'class': 'maintable'})

            author_paste_ids = []

            rows = content_table.find_all('tr')
            for row in rows:  # jesus christ
                cols = row.find_all('td')
                if len(cols) > 4:
                    for td in cols:
                        td_a = td.find('a')
                        if td_a is not None and "archive/text" not in td_a.get('href'):
                            author_paste_ids.append(f"{td_a.get('href').split('/')[1]}")

            for paste_id in author_paste_ids:
                paste_request = get(f"https://pastebin.com/{paste_id}")
                paste_parsed = BeautifulSoup(paste_request.content, features="html.parser")
                paste_title = paste_parsed.find("div", "paste_box_line1").get("title").replace('/', '~')
                raw_link = f"https://pastebin.com/raw/{paste_id}"
                raw_request = get(raw_link)
                raw_parsed = BeautifulSoup(raw_request.content, features="html.parser").text

                with open(f"{self.config.output_folder}/{author_name}/{paste_title}.txt", "w+") as paste_file:
                    print_info(f"Creating file for '{paste_title}'...")
                    paste_file.write(raw_parsed)

    def do_scrape(self):
        paste_links = []
        author_links = []
        print_info("Getting links from archive paste")
        for line in self._archive_lines:  # jesus
            line = re.sub("\s\s+", "", line.lstrip().rstrip())
            if line == "=----= Stories Lost Forever =----=":
                break
            if "https://pastebin.com/" in line and "|" not in line:
                if "-- " not in line:
                    if ": " in line:
                        line = line.split(": ")[1]
                    paste_links.append(line)
                else:
                    if self.config.scrape_authors:
                        author_links.append(line.split("-- ")[1])
        print_info(f"Done. We have {len(paste_links) + len(author_links)} total links.")
        print_general(f"Downloading pastes to {self.config.output_folder}")
        self.create_folder()
        self.do_paste_ripping(paste_links)
        self.do_author_ripping(author_links)
