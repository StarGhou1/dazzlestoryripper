from setuptools import setup, find_packages

setup(
    name='DazzleStoryRipper',
    description='Script that harvests all archived stories from the Dazzle Story Archive paste for local archiving.',
    packages=find_packages(),
    include_package_data=True, install_requires=['requests', 'beautifulsoup4', 'colorama']
)
