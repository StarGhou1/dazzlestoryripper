from os import system
from auxiliary.color_utils import *
from config.dazzle_config import DazzleConfig
from scraping.scraper import Scraper


def print_brand():
    print(f"{Fore.YELLOW}Da{Fore.MAGENTA}zz{Fore.CYAN}le{Fore.WHITE} Paste Story Ripper.{Style.RESET_ALL}")


if __name__ == '__main__':
    config = DazzleConfig()
    exit_loop = False
    while exit_loop is False:
        system("clear")
        print_brand()
        config.print_current_options()
        exit_loop = config.do_options_menu()
    print_general("Commencing scrape operations...")
    scraper = Scraper(config)
    scraper.do_scrape()

